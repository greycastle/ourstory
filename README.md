# ourstory

The new-old way of sharing stories.

Ourstory lets users log in and start co-writing stories by taking turns. You can invite your friends and they will get notifications by email when you have finished your turn. Only the very last part of your writing is shown to your co-writers leading to unexpected and fun plots and twists.

![Example screencast](./example.gif)

## setup

Make sure you've got postgres installed and running on default port.

Then run:

```shell script
bundle exec rake db:migrate
```

First you must make sure the database exists and has a role to run the app, the following will work for running the tests.

```shell script
psql -d postgres -c 'CREATE DATABASE ourstory_test;'
psql -d postgres -c "CREATE USER myapp WITH PASSWORD 'passwd'"
```

## run

```shell script
rails server
```
