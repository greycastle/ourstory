class ChapterAdder < ApplicationService
  def initialize(story, content, author)
    @story = story
    @content = content
    @author = author
  end

  def call
    chapter = Chapter.create(author: @author, content: @content)
    @story.add_chapter chapter
    other_authors = @story.authors.map { |author| author.email } - [@author.email]
    if other_authors.length > 0
      NotificationMailer.with(author_name: @author.name, other_authors: other_authors, story_name: @story.title, story_id: @story.id).notify_new_chapter_email.deliver_later
    end
    chapter.id
  end
end
