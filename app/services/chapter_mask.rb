class ChapterMask < ApplicationService
  def initialize(story, author)
    @story = story
    @author = author
  end

  def call
    @story.chapters.map { |chapter| mask_if_not_author(chapter) }
  end

  private

  def mask_if_not_author(chapter)
    is_author_of?(chapter) ? chapter.content : mask(chapter)
  end

  def is_author_of?(chapter)
    chapter.author == @author
  end

  def mask(chapter)
    chapter.content.gsub(/[^\s]/, "*")
  end
end
