class AuthorInviter < ApplicationService
  def initialize(author_email, author_name, story)
    @author_email = author_email
    @author_name = author_name
    @story = story
  end

  def call
    existing_user = User.find_by_email(@author_email.downcase)
    if existing_user
      InviteMailer.with(user: existing_user, story: @story).invite_email.deliver_later
      return existing_user
    end
    password = "ourstory"
    user = User.create!(password: password, name: @author_name, email: @author_email)
    InviteMailer.with(user: user, story: @story, password: password).invite_user_email.deliver_later
    user
  end
end
