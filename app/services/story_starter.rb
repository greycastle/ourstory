class StoryStarter < ApplicationService
  def initialize(story_beginning)
    @author = story_beginning[:author]
    @title = story_beginning[:title]
    @first_entry = story_beginning[:first_entry]
    @co_authors = story_beginning[:co_authors]
  end

  def call
    story = Story.create(author: @author, title: @title)
    story.add_chapter Chapter.create(author: @author, content: @first_entry)

    @co_authors.each do |co_author|
      story.add_author AuthorInviter.call(co_author[:email], co_author[:name], story)
    end
  end
end
