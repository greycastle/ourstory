class Story < ActiveRecord::Base
  belongs_to :author, class_name: "User", required: true
  has_and_belongs_to_many :authors, class_name: "User"
  has_many :chapters, dependent: :delete_all

  validates_presence_of :title

  after_create :add_original_author

  enum status: [:ongoing, :finished]

  def add_chapter(chapter)
    raise StoryFinishedError if finished?
    raise ChapterForNonAuthorError, chapter.author.email unless authors.include?(chapter.author)
    chapter.story = self
    chapter.save!
  end

  def add_author(author)
    raise AuthorAlreadyAddedError, author.email if authors.include?(author)
    authors << author
    save!
  end

  class StoryFinishedError < StandardError
  end

  class AuthorError < StandardError
    attr_accessor :email

    def initialize(email)
      @email = email
    end
  end

  class AuthorAlreadyAddedError < AuthorError
  end

  class ChapterForNonAuthorError < AuthorError
  end

  private

  def add_original_author
    add_author author
  end
end
