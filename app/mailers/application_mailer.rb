class ApplicationMailer < ActionMailer::Base
  default from: "ourstory@greycastle.se"
  layout "mailer"
end
