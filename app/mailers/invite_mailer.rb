class InviteMailer < ApplicationMailer
  def invite_email
    @user = params[:user]
    @story = params[:story]
    mail(to: @user.email, subject: "#{@story.author.name} wants to write a story with you!")
  end

  def invite_user_email
    @user = params[:user]
    @story = params[:story]
    @password = params[:password]
    mail(to: @user.email, subject: "#{@story.author.name} has invited you to Our Story!")
  end
end
