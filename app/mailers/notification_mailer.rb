class NotificationMailer < ApplicationMailer
  def notify_new_chapter_email
    @author_name = params[:author_name]
    @story_id = params[:story_id]
    @story_name = params[:story_name]
    other_authors = params[:other_authors]
    mail(to: other_authors, subject: "#{@author_name} has continued your story!")
  end
end
