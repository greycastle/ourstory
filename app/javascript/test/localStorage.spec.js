import LocalStorage from '../localStorage'

describe(LocalStorage.name, () => {
  const TEST_KEY = 'testKey'

  afterEach(() => {
    localStorage.removeItem(TEST_KEY)
  })

  test('can crud', () => {
    const storage = new LocalStorage(TEST_KEY)
    expect(storage.get()).toBeNull();

    storage.set({data: 'value'})
    expect(storage.get().data).toEqual('value')

    storage.set({data: 'new value'})
    expect(storage.get().data).toEqual('new value')

    storage.clear();
    expect(storage.get()).toBeNull();
  })
})
