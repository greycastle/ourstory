import { mount } from '@vue/test-utils'
import VueRouter from 'vue-router'
import WriteArticle from '../writeArticle.vue'

describe('WriteArticle', () => {
  test('it can be mounted', () => {
    const wrapper = mount(WriteArticle, { stubs: ['router-link', 'inviteModal', 'top-link', 'content-box'], mocks: { $t: () => 'localized' } })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
