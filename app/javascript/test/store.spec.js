import { mutations, getters } from '../store'

describe('Store', () => {
  const someEmail = 'some@email.com'

  test('can set login', () => {
    const state ={
      email: null
    }
    mutations.login(state, someEmail)

    expect(getters.loggedIn(state)).toBe(true);
    expect(state.email).toBe(someEmail);
  })

  test('can log out', () => {
    const state = {
      email: someEmail,
    }
    mutations.logout(state)

    expect(state.email).toBeNull();
    expect(getters.loggedIn(state)).toBe(false);
  })
})
