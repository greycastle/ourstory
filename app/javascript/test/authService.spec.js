import Store from '../store'
import AuthService from '../authService'

describe(AuthService.name, () => {
  const SOME_EMAIL = 'some@email.com'

  const SOME_TOKEN = {
    token: 'acceess-token',
    client: 'client',
    uid: 'uid',
    expiry: 'expiry'
  }

  const SUCCEESSFUL_AUTH_HEADERS = {
    'access-token': 'access-token',
    'client': 'client',
    'uid': 'uid',
    'expiry': 'expiry'
  }

  const tokenStorage = {
    get: function() { return this.token },
    clear: function() { this.token = null },
    set: function(token) { this.token = token }
  }

  beforeEach(function() {
    Store.commit('logout')
    tokenStorage.token = { token: SOME_TOKEN, email: SOME_EMAIL }
  })

  const interceptorsAddAuthToken = function(service) {
    const config = service.httpClient.interceptors.request.handlers[0].fulfilled({ data: 'request', headers: { } });
    return config.headers['access-token'] == SOME_TOKEN.token;
  }

  test('login is set to store if token exists in token storage', () => {
    new AuthService(Store, tokenStorage);
    expect(Store.getters.loggedIn).toEqual(true);
  })

  test('auth token is added if auth token exists in token storage', () => {
    const service = new AuthService(Store, tokenStorage);
    expect(interceptorsAddAuthToken(service)).toEqual(true);
  })

  test('removes auth token on logout', () => {
    const service = new AuthService(Store, tokenStorage)
    service.logout()

    expect(tokenStorage.token).toBeNull()
    expect(Store.getters.loggedIn).toBe(false)
    expect(interceptorsAddAuthToken(service)).toBe(false)
  })

  test('adds token after login', async () => {
    tokenStorage.token = null

    const service = new AuthService(Store, tokenStorage)

    service.httpClient = {
      post: function(url, data) {
        return Promise.resolve({ headers: SUCCEESSFUL_AUTH_HEADERS })
      }
    }

    await service.login('user', 'password')

    expect(tokenStorage.get().email).toEqual('user')
    expect(Store.getters.loggedIn).toBe(true)
  })

  test('adds token after signup', async () => {
    tokenStorage.token = null

    const service = new AuthService(Store, tokenStorage)

    service.httpClient = {
      post: function(url, data) {
        return Promise.resolve({ headers: SUCCEESSFUL_AUTH_HEADERS })
      }
    }

    await service.register('name', 'email')

    expect(tokenStorage.get().email).toEqual('email')
    expect(Store.getters.loggedIn).toBe(true)
  })
})
