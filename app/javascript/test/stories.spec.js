import { shallowMount } from '@vue/test-utils'
import Vue from 'vue'
import { default as Stories, helpers } from '../stories.vue'

describe(Stories.name, () => {

  describe('mounting', () => {

    const AUTHOR_ME = { email: 'my@email.com', name: 'me' }
    const AUTHOR_OTHER = { email: 'other@email.com', name: 'other' }

    const STORY_LAST_AUTHOR_ME = {
      lastAuthor: {
        id: AUTHOR_ME.email,
        name: AUTHOR_ME.name
      },
      id: 'id1',
      title: 'Story by me',
      lastEntry: 'some text',
      authors: [ AUTHOR_OTHER, AUTHOR_ME ]
    }
    const STORY_LAST_AUTHOR_OTHER = {
      lastAuthor: {
        id: AUTHOR_OTHER.email,
        name: AUTHOR_OTHER.name
      },
      id: 'id2',
      title: 'Story from other author',
      lastEntry: 'some text',
      authors: [ AUTHOR_OTHER, AUTHOR_ME ]
    }

    const $postService = {
      stories: [ STORY_LAST_AUTHOR_ME, STORY_LAST_AUTHOR_OTHER ],
      getStories: function() { return Promise.resolve(this.stories) }
    }

    const stubs = [ 'router-link', 'ripped-box' ]
    const mocks = { $t: () => 'localized', $postService, $store: { state: { email: AUTHOR_ME.email } } }

    let wrapper;

    const getStory = function(findStory) {
      return wrapper.vm.stories.filter((story) => story.id === findStory.id)[0];
    }

    beforeEach(async () => {
      wrapper = shallowMount(Stories, { stubs, mocks })
      await Vue.nextTick();
    })

    test('loads stories on mount', async () => {
      expect(wrapper.vm.stories.length).toEqual($postService.stories.length)
    })

    test('stories shows last author', async () => {
      expect(getStory(STORY_LAST_AUTHOR_ME).lastAuthor).toEqual(AUTHOR_ME.name)
      expect(getStory(STORY_LAST_AUTHOR_OTHER).lastAuthor).toEqual(AUTHOR_OTHER.name)
    })

    test('other authors exclude myself', async () => {
      expect(getStory(STORY_LAST_AUTHOR_ME).authorNames).toEqual([ AUTHOR_OTHER.name ])
      expect(getStory(STORY_LAST_AUTHOR_ME).authorNames).toEqual([ AUTHOR_OTHER.name ])
    })
  })

  describe('helpers', () => {
    test('can compare author', () => {
      expect(helpers.isAuthorMe('my@email.com', 'other@email.com')).toBe(false)
      expect(helpers.isAuthorMe('my@email.com', 'my@email.com')).toBe(true)
      expect(helpers.isAuthorMe('MY@EMAIL.com', 'my@email.com')).toBe(true)
    })
  })
})
