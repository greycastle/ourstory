export default class LocalStorage {
  constructor(key) {
    this.key = key;
  }

  get() {
    return JSON.parse(localStorage.getItem(this.key))
  }

  clear() {
    localStorage.removeItem(this.key)
  }

  set(data) {
    localStorage.setItem(this.key, JSON.stringify(data))
  }
}
