const locales = {
  en: {
    write: {
      titlePlaceholder: 'this is my story title',
      displayedToNextPerson: 'This will be displayed to the next author:',
      toNextAuthor: 'Pass to the next author'
    },
    inviteModal: {
      authorEmail: 'Co-author email',
      authorName: 'Co-author name',
      add: 'Add author',
      send: 'Send invites',
      title: 'Who would you like to write with?'
    },
    stories: {
      title: "Stories",
      newStory: 'Begin new story',
      lastWriter: "Last writer",
      otherWriters: "Other writers",
      edit: 'Modify your last writing',
      continue: 'Your turn! Continue the story'
    },
    logout: {
      title: 'See you again!'
    },
    signUp: {
      title: "Who are you?",
      name: "My name is",
      namePlaceholder: "Jo Doh",
      email: 'My email is',
      emailPlaceholder: "jo.do@email.com",
      begin: 'Begin writing',
      login: "I've written here before!"
    },
    login: {
      title: "Welcome back!",
      email: 'Email',
      emailPlaceholder: "my@email.com",
      password: 'Password',
      login: 'Login',
      failure: 'Failed to login, please try again.'
    },
    story: {
      loading: 'Loading..',
      displayedToNextPerson: 'This will be displayed to the next author:',
      toNextAuthor: 'Pass to the next author',
      loadError: "Failed to fetch the story, please try again later",
      lastContentTitle: 'Last written:',
      storyThisFar: 'The story this far',
      by: 'by',
      continue: 'Continue here'
    }
  },
  sv: {
    write: {
      titlePlaceholder: 'min sagas titel',
      displayedToNextPerson: 'Det här kommer nästa författare se:',
      toNextAuthor: 'Skicka till nästa författare'
    },
    inviteModal: {
      authorEmail: 'Medförfattares epost',
      authorName: 'Medförfattares namn',
      add: 'Lägg till',
      send: 'Skicka inbjudan',
      title: 'Vem vill du skriva med?'
    },
    stories: {
      title: 'Skriverier',
      newStory: 'Skriv en ny historia',
      lastWriter: "Senast skriven av",
      otherWriters: "Andra författare",
      edit: 'Ändra vad du skrev sist',
      continue: "Din tur! Fortsätt historien"
    },
    logout: {
      title: 'Tills nästa gång!'
    },
    signUp: {
      title: "Vem är du?",
      name: "Jag heter",
      namePlaceholder: "Jo Vem",
      email: 'Min epost är',
      emailPlaceholder: "jo.vem@epost.se",
      begin: 'Börja skriv',
      login: "Jag har skrivit här förut!"
    },
    login: {
      title: "Välkommen tillbaka!",
      email: 'Epost',
      emailPlaceholder: "min@epost.se",
      password: 'Lösenord',
      login: 'Logga in',
      failure: 'Misslyckades logga in, försök igen.'
    },
    story: {
      loading: 'Laddar..',
      displayedToNextPerson: 'Det här kommer nästa författare se:',
      toNextAuthor: 'Skicka till nästa författare',
      loadError: "Kunde inte läsa historien, försök igen senare",
      lastConentTitle: 'Senast skrivet:',
      by: 'av ',
      storyThisFar: 'Berättelsen hitintills',
      continue: 'Fortsätt här'
    }
  }
}

export default locales;
