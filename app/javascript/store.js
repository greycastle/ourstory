import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const mutations = {
  login: function(store, email) {
    store.email = email;
  },
  logout: function (store) {
    store.email = null;
  }
}

export const getters = {
  loggedIn: state => {
    return state.email != null;
  }
}

export default new Vuex.Store({
  state: {
    email: null
  },
  getters,
  mutations
});
