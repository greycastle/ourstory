import Vue from 'vue'

Vue.mixin( {
  beforeCreate() {
    const options = this.$options;
    if ( options.postService )
      this.$postService = options.postService;
    else if ( options.parent && options.parent.$postService )
      this.$postService = options.parent.$postService;
  }
} );

const STORIES_URL = '/api/stories';

export default class PostService {

  constructor(httpClient) {
    this.httpClient = httpClient;
  }

  async postResponse(title, content, coAuthors) {
    const data = {
      title: title,
      'first_entry': content,
      'co_authors': coAuthors
    }
    return await this.httpClient.post(STORIES_URL, data);
  }

  async postNext(storyId, content) {
    await this.httpClient.post(`${STORIES_URL}/${storyId}/chapters`, { content });
  }

  async getStory(id) {
    const response = await this.httpClient.get(`${STORIES_URL}/${id}`);
    return response.data;
  }

  async getStories() {
      const response = await this.httpClient.get(STORIES_URL + '/all');
      return response.data;
  }
}
