import Axios from "axios";
import Vue from 'vue'

Vue.mixin( {
  beforeCreate() {
    const options = this.$options;
    if ( options.authService )
      this.$authService = options.authService;
    else if ( options.parent && options.parent.$authService )
      this.$authService = options.parent.$authService;
  }
} );

function getToken(headers) {
  return {
    token: headers['access-token'],
    client: headers['client'],
    uid: headers['uid'],
    expiry: headers['expiry']
  }
}

function addAuthHeaders(config, token) {
  config.headers = {
    'token-type': 'Bearer',
    'access-token': token.token,
    'client': token.client,
    'uid': token.uid,
    'expiry': token.expiry,
    ...config.headers
  }
}

function interceptRequest(config, token) {
  if (token) {
    addAuthHeaders(config, token)
  }
  return config
}

const AUTH_URL = '/api/auth'

export default class AuthService {
  constructor(store, tokenStorage) {
    this.onExpiry = null;
    this.store = store;
    this.tokenStorage = tokenStorage;

    const currentLogin = tokenStorage.get();
    if (currentLogin) {
      this.store.commit('login', currentLogin.email);
      this.token = currentLogin.token
    }

    this.httpClient = Axios.create()
    this.httpClient.interceptors.request.use((config) => interceptRequest(config, this.token), (error) => Promise.reject(error));
    this.httpClient.interceptors.response.use((success) => success, this.handleRequestError.bind(this));
  }

  handleExpiry() {
    this.tokenStorage.clear();
    if (this.onExpiry != null) {
      this.onExpiry();
    }
  }

  handleRequestError(error) {
    if (error.response.status == 401) {
     this.handleExpiry();
    }
    return Promise.reject(error);
  }

  async onLoginExpired(callback) {
    this.onExpiry = callback;
  }

  async login(username, password) {
    const response = await this.httpClient.post(AUTH_URL + '/sign_in', { email: username, password: password });
    this._setLoginDetailsFromHeaders(username, response.headers)
  }

  async register(name, email) {
    const response = await this.httpClient.post(AUTH_URL + '/register', { name, email });
    this._setLoginDetailsFromHeaders(email, response.headers)
  }

  logout() {
    this.tokenStorage.clear();
    this.token = null;
    this.store.commit('logout');
  }

  _setLoginDetailsFromHeaders(username, headers) {
    this.token = getToken(headers);
    this.tokenStorage.set({
      email: username,
      token: this.token
    })
    this.store.commit('login', username)
  }
}
