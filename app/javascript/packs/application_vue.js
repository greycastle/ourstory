/* eslint no-console: 0 */
import Vue from 'vue'
import Router from "vue-router";
import VueI18n from 'vue-i18n'
import Landing from '../landing.vue'
import App from '../app.vue';
import SignUp from '../signUp.vue';
import NotFound from '../notFound.vue';
import WriteArticle from '../writeArticle.vue';
import ContinueStory from '../continueStory.vue';
import Stories from '../stories.vue';
import Login from '../login.vue';
import Logout from '../logout.vue';
import Modal from '../modal.vue';
import InviteModal from '../inviteModal.vue';
import ContentBox from '../contentBox.vue';
import TopLink from '../topLink.vue';
import store from '../store';
import Locales from '../locales';
import AuthService from '../authService';
import LocalStorage from '../localStorage'
import PostService from '../postService';
import RippedBox from '../rippedBox';

const useAuth = {
  meta: {
    requiresAuth: true
  }
}

const routes = [
  { path: '/', component: Landing },
  { path: '/register', component: SignUp },
  { path: '/logout', component: Logout },
  { path: '/login', component: Login },
  { path: '/new-story', component: WriteArticle, ...useAuth },
  { path: '/stories/:id', component: ContinueStory, ...useAuth },
  { path: '/stories', component: Stories, ...useAuth },
  { path: '*', component: NotFound }
]

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes,
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta && record.meta.requiresAuth) && !store.getters.loggedIn) {
    console.log('Not logged in, redirecting..')
    next({
        path: '/login',
        query: { nextUrl: to.fullPath }
    });
  } else {
    next();
  }
});

Vue.component('modal', Modal);
Vue.component('top-link', TopLink);
Vue.component('inviteModal', InviteModal);
Vue.component('content-box', ContentBox);
Vue.component('ripped-box', RippedBox);

Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: navigator.language.split('-')[0],
  fallbackLocale: 'en',
  messages: Locales
})

const authService = new AuthService(store, new LocalStorage('TOKEN'));
authService.onLoginExpired(() => { window.location = '/login';  })
const postService = new PostService(authService.httpClient);

new Vue({
  i18n,
  router,
  store,
  postService: postService,
  authService,
  render: h => h(App)
}).$mount('#app')
