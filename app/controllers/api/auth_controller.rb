class Api::AuthController < BaseController
  before_action :authenticate_user!, only: :logout

  def logout
    Devise.sign_out_all_scopes
    redirect_to '/'
  end

  def register
    email = params[:email].downcase
    name = params[:name]

    existing_user = User.find_by_email(email)
    if existing_user.present?
      Rails.logger.error "User already exists: #{email}"
      render json: { error: 'USER_EXISTS', status: 'ERROR' }, status: 409
      return
    end

    password = 'ourstory'
    user = User.create!(email: email, name: name, password: password)
    token = user.create_new_auth_token
    response.headers.merge!(token)

    render json: { status: 'SUCCESS' }, status: 201
  end
end
