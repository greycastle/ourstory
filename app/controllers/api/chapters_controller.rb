class Api::ChaptersController < BaseController
  before_action :authenticate_user!

  def create
    id, content = params.require([:id, :content])
    story = Story.find_by_id(id)
    if story.nil?
      render json: { error: "Not found" }, status: 404
      return
    end

    chapter_id = ChapterAdder.call(story, content, current_user)
    render plain: chapter_id, status: 201
  end
end
