class Api::StoriesController < BaseController
  before_action :authenticate_user!

  def create
    co_authors = params[:co_authors].map do |author|
      {
        email: author[:email],
        name: author[:name],
      }
    end
    new_story_id = StoryStarter.call(author: current_user, title: params[:title], first_entry: params[:first_entry], co_authors: co_authors)
    render json: { id: new_story_id }, status: 201
  end

  def show
    story = Story.find_by_id(params[:id])
    render json: { error: "Not found" }, status: 404 if story.nil?

    last_chapter = story.chapters.last
    last_author = last_chapter.author
    render_data = {
      title: story.title,
      last_author: {
        name: last_author.name,
        email: last_author.email,
      },
      chapters: ChapterMask.call(story, current_user),
      last_entry: last_chapter.content.last(100),
    }
    render json: render_data
  end

  def all
    stories = current_user.stories.map do |story|
      {
        id: story.id,
        title: story.title,
        lastEntry: story.chapters.last.content,
        authors: story.authors.map { |author| { name: author.name, email: author.email } },
        lastAuthor: {
          name: story.chapters.last.author.name,
          id: story.chapters.last.author.email,
        },
      }
    end
    render json: stories
  end
end
