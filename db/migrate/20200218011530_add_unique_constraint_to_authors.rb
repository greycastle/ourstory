class AddUniqueConstraintToAuthors < ActiveRecord::Migration[5.2]
  def change
    add_index :stories_users, [:story_id, :user_id], :unique => true
  end
end
