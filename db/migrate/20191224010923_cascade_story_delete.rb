class CascadeStoryDelete < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :chapters, name: "story"
    add_foreign_key :chapters, :stories, column: "story_id", name: "story", on_delete: :cascade
  end
end
