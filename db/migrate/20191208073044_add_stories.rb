class AddStories < ActiveRecord::Migration[5.2]
  def change
    create_table :stories do |t|
      t.string :title
      t.integer :author_id
      t.timestamps
    end

    add_foreign_key :stories, :users, column: "author_id", name: "author"

    create_join_table :stories, :users do |t|
      t.index :story_id
      t.index :user_id
    end

    create_table :chapters do |t|
      t.text :content
      t.integer :author_id
      t.integer :story_id
      t.timestamps
    end

    add_foreign_key :chapters, :users, column: "author_id", name: "author"
    add_foreign_key :chapters, :stories, column: "story_id", name: "story"
  end
end
