# Preview all emails at http://localhost:3000/rails/mailers/invite_mailer
class InviteMailerPreview < ActionMailer::Preview
  def invite_email
    InviteMailer.with(user: User.first, story: Story.first).invite_email
  end

  def invite_user_email
    InviteMailer.with(user: User.first, story: Story.first, password: "3xamplepassw0rd").invite_user_email
  end
end
