require "rails_helper"

RSpec.describe NotificationMailer, type: :mailer do
  it "should send the email to all related authors" do
    users = (1..2).map { |x| build(:user) }
    story = build(:story, author: users.first, authors: users)

    parameters = { author_name: users.first.name, story_id: story.id, story_name: story.title, other_authors: [users.last.email] }
    mail = NotificationMailer.with(parameters).notify_new_chapter_email

    expect(mail.to).to eq([users.last.email])
  end
end
