require "rails_helper"

RSpec.describe InviteMailer, type: :mailer do
  describe "#invite_email" do
    let (:chapter_content) { "some content" }
    let (:user) { build(:user) }
    subject do
      story = create(:story, author: user)
      story.add_chapter build(:chapter, author: user, content: chapter_content)
      mail = InviteMailer.with(user: user, story: story).invite_email
    end

    it "should include author name in the subject" do
      expect(subject.subject).to include(user.name)
    end

    it "should include the content excerpt" do
      expect(subject.body).to include(chapter_content)
    end
  end
end
