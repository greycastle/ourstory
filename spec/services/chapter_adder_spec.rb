require "rails_helper"

RSpec.describe ChapterAdder, :type => :service do
  before :each do
    @author = create(:user)
    @story = create(:story, author: @author)
  end

  it "can add a chapter to a story" do
    chapter_id = ChapterAdder.call(@story, "some content", @author)
    chapter = Chapter.find_by_id(chapter_id)
    expect(chapter.story_id).to eq(@story.id)
    expect(chapter.content).to eq("some content")
    expect(chapter.author).to eq(@author)
  end

  it "sends a message to all other authors when a story is continued" do
    another_author = create(:user)
    @story.add_author another_author
    chapter_id = ChapterAdder.call(@story, "some content", another_author)

    mail = ActionMailer::Base.deliveries.first
    expect(mail).not_to be_nil
    expect(mail.to).to eq([@author.email])
  end
end
