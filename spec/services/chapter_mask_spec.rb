require "rails_helper"

RSpec.describe ChapterMask, :type => :service do
  before :each do
    @author = create(:user)
    @other_author = create(:user)
    @story = create(:story, author: @author)
    @story.add_author @other_author
    @story.add_chapter build(:chapter, author: @author, content: "chapter 1 by author")
    @story.add_chapter build(:chapter, author: @other_author, content: "chapter 2 by another author")
    @story.add_chapter build(:chapter, author: @author, content: "chapter 3 by author")
  end

  it "masks all non whitespace characters" do
    chapters = ChapterMask.call(@story, @author)
    masked = chapters[1]
    expect(masked).to eq("******* * ** ******* ******")
  end

  it "only masks non-author entries" do
    chapters = ChapterMask.call(@story, @author)
    expect(chapters[0]).to eq(@story.chapters[0].content)
    expect(chapters[2]).to eq(@story.chapters[2].content)
  end
end
