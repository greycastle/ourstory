require "faker"

FactoryBot.define do
  factory :story, class: Story do
    title { Faker::Lorem.sentence }
  end
end
