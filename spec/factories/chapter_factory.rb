require "faker"

FactoryBot.define do
  factory :chapter, class: Chapter do
    content { Faker::Lorem.paragraph(sentence_count: 2) }
  end
end
