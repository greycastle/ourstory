require "rails_helper"

RSpec.feature "Sign up", :type => :feature do
  scenario "Signs up by writing a new story", :ui do
    visit "/"

    click_link "Begin our story"

    fill_in "nameInput", with: "James"
    fill_in "emailInput", with: "james@greycastle.se"
    click_button "Begin writing"

    fill_in "titleInput", with: "Story title"
    fill_in "content", with: "Story content"
    click_button "Pass to the next author"

    fill_in "coAuthorEmail", with: "david@greycastle.se"
    fill_in "coAuthorName", with: "David"
    click_button "Add author"
    click_button "Send invites"

    find("p", text: "Story title")
  end
end
