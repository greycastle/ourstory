require "rails_helper"

RSpec.feature "Sign in", :type => :feature do
  before do
    @user = create(:user, password: "temp password")
    co_author = create(:user)
    @story = create(:story, author: co_author)
    @story.add_chapter build(:chapter, author: co_author, content: "first chapter")
    @story.add_author @user
  end

  scenario "Can sign in as an existing user and continue a story", :ui do
    visit "/"

    click_link "Begin our story"
    click_link "I've written here before"

    fill_in "email", with: @user.email
    fill_in "password", with: "temp password"
    click_button "Login"

    click_link "Your turn! Continue the story"

    fill_in "content", with: "my chapter"
    click_button "Pass to the next author"

    find("a", text: "Modify your last writing")
  end

  scenario "Can continue story from link", :ui do
    visit "/stories/#{@story.id}"

    fill_in "email", with: @user.email
    fill_in "password", with: "temp password"
    click_button "Login"

    fill_in "content", with: "my chapter"
    click_button "Pass to the next author"

    find("a", text: "Modify your last writing")
  end
end
