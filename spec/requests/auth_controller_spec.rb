require "rails_helper"

RSpec.describe Api::AuthController, :type => :request do
  describe "#logout" do
    it "redirects to root page after logout" do
      user = create(:user)
      delete "/api/auth/sign_out", headers: auth_headers(user)
      expect(response).to have_http_status 200
    end
  end

  describe "#login" do
    it "removes the active token" do
      user = create(:user)
      post "/api/auth/sign_in", params: { email: user.email, password: user.password }
      expect(response).to have_http_status(200)
      expect(response.headers["access-token"]).not_to be_nil
    end
  end

  describe "#register" do
    it "cannot register two users with the same email" do
      duplicate_email = "this@email.com"
      password = "randompass"
      post "/api/auth/register", params: { email: duplicate_email, name: "First Name", password: password, password_confirmation: password }
      post "/api/auth/register", params: { email: duplicate_email, name: "Second Name", password: password, password_confirmation: password }
      expect(response).to have_http_status(409)
    end

    it "returns a login token for registered user" do
      user_email = "my@user.com"
      post "/api/auth/register", params: { email: user_email, name: "my name" }
      expect(response).to have_http_status(201)
      user = User.find_by_email(user_email)
      expect(user.name).to eq("my name")
      expect(response.headers["token-type"]).to eq("Bearer")
    end
  end
end
