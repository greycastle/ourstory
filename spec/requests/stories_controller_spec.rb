require "rails_helper"

RSpec.describe Api::StoriesController, :type => :request do
  STORIES_URL = "/api/stories"

  before :each do
    @user = create(:user)
  end

  describe "#create" do
    it "can create story for user" do
      co_author_email = "some@user.com"
      story_data = {
        title: "story title",
        first_entry: "some text",
        co_authors: [
          {
            email: co_author_email,
            name: "Some User",
          },
        ],
      }
      post STORIES_URL, params: story_data, headers: auth_headers(@user)
      expect(response).to have_http_status 201

      story = Story.find_by_title(story_data[:title])
      expect(story.chapters.first.content).to eq(story_data[:first_entry])

      # and user is added
      added_user = User.find_by_email(co_author_email)
      expect(added_user).not_to be_nil
      expect(added_user.name).to eq("Some User")

      # and author is added
      expect(story.authors.map(&:email)).to include(co_author_email)
    end
  end

  describe "#show" do
    it "can retrieve a single story" do
      user = create(:user)
      story = create(:story, author: user, title: "title")
      long_content = "Start " + (1..500).map { |x| x.to_s }.join(" ") + " but end"
      story.add_chapter build(:chapter, author: user, content: long_content)

      get STORIES_URL + "/#{story.id}", headers: auth_headers(user)
      expect(response).to have_http_status(200)
      response_body = JSON.parse(response.body)
      expect(response_body["title"]).to eq("title")
      expect(response_body["last_author"]["email"]).to eq(user.email)
      expect(response_body["last_author"]["name"]).to eq(user.name)

      # last hundred
      expect(response_body["last_entry"]).not_to start_with("Start")
      expect(response_body["last_entry"]).to end_with("499 500 but end")
    end

    it "shows chapters only for the author" do
      user = create(:user)
      other_user = create(:user)
      story = create(:story, author: user, title: "title")
      story.add_author other_user
      story.add_chapter build(:chapter, author: user, content: "first")
      story.add_chapter build(:chapter, author: other_user, content: "second chapter")
      story.add_chapter build(:chapter, author: user, content: "third")

      get STORIES_URL + "/#{story.id}", headers: auth_headers(user)
      response_body = JSON.parse(response.body)

      expect(response_body["chapters"][0]).to eq("first")
      expect(response_body["chapters"][1]).to eq("****** *******")
      expect(response_body["chapters"][2]).to eq("third")
    end
  end

  describe "#all" do
    before :each do
      @user = create(:user)
      @story = create(:story, author: @user)
      @story.add_chapter build(:chapter, author: @user, content: "content")
      @authenticated_user = @user
    end

    subject do
      get STORIES_URL + "/all", headers: auth_headers(@authenticated_user)
      JSON.parse(response.body)
    end

    it "returns all authors" do
      @story.add_author create(:user)
      @story.add_author create(:user)

      authors = subject.first["authors"]
      expect(authors.length).to be(3)
      expect(authors.map { |a| a["name"] }).to include(@user.name)
      expect(authors.map { |a| a["email"] }).to include(@user.email)
    end

    it "can get stories for a user" do
      expect(subject.length).to eq(1)
      expect(subject[0]["id"]).to eq(@story.id)
    end

    it "can get stories a user is participating in" do
      participating_author = create(:user)
      @authenticated_user = participating_author
      @story.add_author participating_author

      expect(subject.length).to eq(1)
      expect(subject[0]["id"]).to eq(@story.id)
    end
  end
end
