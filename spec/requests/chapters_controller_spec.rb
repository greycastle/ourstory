require "rails_helper"

RSpec.describe Api::ChaptersController, :type => :request do
  before :each do
    @user = create(:user)
    @story = create(:story, author: @user, title: "our story")
    @story.add_chapter create(:chapter, author: @user, content: "some content", story: @story)
  end

  describe "#create" do
    it "can create chapter for story" do
      chapter_data = {
        content: "next part",
      }
      post "/api/stories/#{@story.id}/chapters", params: chapter_data, headers: auth_headers(@user)
      expect(response).to have_http_status 201

      @story.reload
      chapter = @story.chapters.last
      expect(chapter.content).to eq(chapter_data[:content])
      expect(response.body).to eq(chapter.id.to_s)
    end
  end
end
