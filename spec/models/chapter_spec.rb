require "rails_helper"

RSpec.describe Chapter, :type => :model do
  describe "#valid" do
    subject { Chapter.new }
    let(:user) { create(:user) }
    let(:story) { Story.new }

    it "is not valid without an author" do
      subject.story = story
      expect(subject).to be_invalid
    end

    it "is not valid without a story" do
      subject.author = user
      expect(subject).to be_invalid
    end
  end
end
