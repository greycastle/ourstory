require "rails_helper"

RSpec.describe Story, :type => :model do
  describe "#valid" do
    let(:user) { create(:user) }
    let(:chapter) { build(:chapter, author: user) }
    subject { Story.new }

    it "is not valid without an author" do
      subject.title = Faker::Lorem.sentence
      expect(subject).to be_invalid
    end

    it "is not valid without an title" do
      subject.author = user
      expect(subject).to be_invalid
    end

    it "adds the story author to authors" do
      subject.author = user
      subject.title = Faker::Lorem.sentence
      subject.save!
      expect(subject.authors).to include(subject.author)
    end
  end

  describe "#add_author" do
    let(:user) { create(:user) }
    subject { create(:story, author: user) }

    it "throws exception if adding author twice" do
      expect { subject.add_author user }.to raise_error(Story::AuthorAlreadyAddedError)
    end

    it "validates database integrity" do
      expect { subject.authors << user }.to raise_error(ActiveRecord::RecordNotUnique)
    end

    it "adds story to user stories" do
      new_user = build(:user)
      subject.add_author new_user
      expect(new_user.stories.map(&:id)).to include(subject.id)
    end
  end

  describe "#add_chapter" do
    let(:user) { create(:user) }
    subject { create(:story, title: "test", author: user) }

    it "adds chapter to story" do
      subject.add_chapter Chapter.new author: user, content: 'content'
      expect(subject.chapters.length).to eq(1)
    end

    it "throws error on adding chapter" do
      subject.finished!
      expect { subject.add_chapter Chapter.new }.to raise_error(Story::StoryFinishedError)
    end

    it "throws error on adding chapter from non-author" do
      new_user = build(:user)
      chapter = Chapter.new author: new_user, content: 'content'
      expect { subject.add_chapter chapter }.to raise_error(Story::ChapterForNonAuthorError)
    end
  end

  describe "#status" do
    let(:user) { create(:user) }
    subject { Story.new title: "test", author: user }

    describe "#ongoing" do
      it "is ongoing by default" do
        expect(subject.ongoing?).to be(true)
      end

      it "can finish" do
        subject.finished!
        expect(subject.ongoing?).to be(false)
        expect(subject.finished?).to be(true)
      end
    end

    describe "#finished" do
      it "can revert back to ongoing" do
        subject.finished!
        subject.ongoing!
        expect(subject.ongoing?).to be(true)
        expect(subject.finished?).to be(false)
      end
    end
  end
end
