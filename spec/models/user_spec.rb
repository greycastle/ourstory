require "rails_helper"

RSpec.describe User, :type => :model do
  describe "#valid" do
    subject { User.new }

    it "is not valid without an email" do
      subject.name = "Bob Jones"
      expect(User.new).to be_invalid
    end

    it "is not valid without a name" do
      subject.email = "bob@jones.com"
      expect(subject).to be_invalid
    end
  end

  describe "#stories" do
    subject { create(:user) }
    it "can list all user stories" do
      story = create(:story, author: subject)
      expect(subject.stories.length).to eq(1)
    end
  end
end
