module.exports = {
  clearMocks: true,

  collectCoverage: true,
  coverageDirectory: "coverage",
  coverageReporters: [ "lcov", "html", "text-summary" ],
  roots: [ 'app/javascript/test/' ],
  moduleFileExtensions: [
    "js",
    "json",
    "vue",
  ],

  transform: {
    ".*\\.(vue)$": "vue-i18n-jest",
    "^.+\\.js$": "<rootDir>/node_modules/babel-jest"
  },
  testEnvironment: "jsdom"
};
