Rails.application.routes.draw do
  mount_devise_token_auth_for "User", at: "api/auth"

  root :to => "application#index"

  get "/", to: "application#index"

  namespace :api do
    resources :auth do
      collection do
        get :logout
        post :register
      end
    end

    resources :stories, only: %i(create all show) do
      collection do
        get :all
        post "/:id/chapters", to: "chapters#create"
      end
    end
  end

  # debugging emails in dev
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

  # let Vue handle 404s
  get "*path", to: "application#index"
end
